extends Area2D

class_name ShipClass

var Bullet = preload("res://scenes/bullet/Bullet.tscn")
var Mine = preload("res://scenes/mine/Mine.tscn")
var pointer = preload("res://scenes/aimingpointer/AimingPointer.tscn")

#per-ship stats
var speed = 300
var maxammo = 3
var maxmine = 3
var bulletspeedmodifier = 1
var bulletlifemodifier = 1
var bulletdamagemodifier = 1
var maxhealth = 1200
var description = ""
var bullet_number = 1
var prj_spawn_x = 0

#ship-shared stats
var acc_vector = Vector2(0.5,0)
var activebullets = 0
var activemines = 0
var velocity = Vector2()
var movement = Vector2()
var ACC = 0.03
var screen_size
var health = 1200
var can_fire = false
var firerange = 0
var bullet_duration = 0
var reloading = false
var bullet_life_array:Array = []
var pointer_node = null
enum MODE {LOCAL, AI, NETWORK, NETWORK_SLAVE}
var current_mode = MODE.LOCAL
var firing = false

#networking
slave var slave_velocity = Vector2()
slave var slave_position = Vector2()
slave var slave_rotation = 0

signal destroyed(player)
signal health_changed(hp)
signal shoot(bullet)
signal reloading(state)
signal fire

func _init(
		ship_speed,
		ship_maxammo,
		ship_bulletspeedmodifier,
		ship_bulletlifemodifier,
		ship_bulletdamagemodifier,
		ship_maxhealth,
		ship_description,
		ship_prj_spawn_x,
		ship_bullet_number
	):
	speed = ship_speed
	maxammo = ship_maxammo
	bulletspeedmodifier = ship_bulletspeedmodifier
	bulletlifemodifier = ship_bulletlifemodifier
	bulletdamagemodifier = ship_bulletdamagemodifier
	maxhealth = ship_maxhealth
	description = ship_description
	prj_spawn_x = ship_prj_spawn_x
	bullet_number = ship_bullet_number


"""--- general ship managment---"""

func _ready():
	
	#general code
	hide()
	$HealthBar.hide()
	screen_size = get_viewport_rect().size
	var tempbullet = Bullet.instance()
	firerange = (tempbullet.speed*bulletspeedmodifier*tempbullet.maxlife*bulletlifemodifier)+prj_spawn_x
	bullet_duration = tempbullet.maxlife * bulletlifemodifier
	tempbullet.free()
	$HealthBar.max_value = maxhealth
	$AnimatedSprite.animation = "idle"
	$AnimatedSprite.play()
	self.connect("area_entered",self,"_on_ship_entered")
	self.connect("fire",self,"fire")
	for i in range(maxammo):
		bullet_life_array.append(0.0)
	
	#player-controlled only code
	if([MODE.LOCAL,MODE.NETWORK].has(current_mode)):
		pointer_node = pointer.instance()
		add_child(pointer_node)
		pointer_node.hide()
		self.connect("reloading",pointer_node,"set_reloading")

func show_hp_bar(shown:bool):
	if shown:
		$HealthBar.show()
	else:
		$HealthBar.hide()

func set_as_ai():
	current_mode = MODE.AI

func set_as_networked(ismaster:bool):
	if ismaster:
		current_mode = MODE.NETWORK
	else:
		current_mode = MODE.NETWORK_SLAVE

func start(pos):
	velocity = Vector2()
	movement = Vector2()
	firing = false
	can_fire = true
	position = pos
	health = maxhealth
	emit_signal("health_changed",health)
	set_process(true)
	show()
	if([MODE.LOCAL,MODE.NETWORK].has(current_mode)):
		pointer_node.show()
		pointer_node.reset()
	$CollisionShape2D.call_deferred("set_disabled", false)

func end(destroyed = true):
	if destroyed:
		emit_signal("destroyed",$".")
	hide()
	if([MODE.LOCAL,MODE.NETWORK].has(current_mode)):
		pointer_node.hide()
	set_process(false)
	can_fire = false
	$CollisionShape2D.call_deferred("set_disabled", true)

func freeze(state:bool) -> void:
	set_process(!state)
	set_physics_process(!state)
	set_process_input(!state)
	if([MODE.LOCAL,MODE.NETWORK].has(current_mode)):
		if pointer_node != null:
			pointer_node.freeze(state)
		else:
			print_debug("attempting to freeze a non-initialised ship pointer")

"""--- general processing---"""

func _process(delta): 
	if([MODE.NETWORK,MODE.LOCAL].has(current_mode)):
		if firing:
			fire()

func _physics_process(delta):
	if([MODE.AI,MODE.LOCAL].has(current_mode)):
		local_process(delta)
	elif([MODE.NETWORK,MODE.NETWORK_SLAVE].has(current_mode)):
		networked_process(delta)

func local_process(delta):
	for i in range(maxammo):
		bullet_life_array[i] -= delta
	
	
	#handling speed
	if velocity.length() > 0:
		velocity = velocity.normalized()
		var acc_dir = acc_vector.rotated(rotation)+velocity
		movement = movement.linear_interpolate(velocity, ACC*acc_dir.length())
		$AnimatedSprite.animation = "move"
	else:
		$AnimatedSprite.animation = "idle"
	
	#handling rotation controls PLAYER ONLY
	if([MODE.LOCAL,MODE.NETWORK].has(current_mode)):
		pointer_node.ship_position = position
		var ship_pointer_vec = pointer_node.aiming_position - position
		rotation = ship_pointer_vec.angle()
		if ship_pointer_vec.length() > firerange:
			pointer_node.set_away(true)
		else:
			pointer_node.set_away(false)
	
	position += movement * speed * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func networked_process(delta):
	if is_network_master():
		pointer_node.ship_position = position
		var ship_pointer_vec = pointer_node.aiming_position - position
		var in_rotation = ship_pointer_vec.angle()
		if ship_pointer_vec.length() > firerange:
			pointer_node.set_away(true)
		else:
			pointer_node.set_away(false)
		rset_unreliable("slave_position",position)
		rset("slave_rotation", rotation)
		rset("slave_velocity", velocity)
		networked_applystate(velocity,in_rotation,delta)
	else:
		networked_applystate(slave_velocity,slave_rotation,delta)
		position = slave_position

func networked_applystate(velocity,input_rotation,delta):
	for i in range(maxammo):
		bullet_life_array[i] -= delta
	#handling speed
	if velocity.length() > 0:
		velocity = velocity.normalized()
		var acc_dir = acc_vector.rotated(rotation)+velocity
		movement = movement.linear_interpolate(velocity, ACC*acc_dir.length())
		$AnimatedSprite.animation = "move"
	else:
		$AnimatedSprite.animation = "idle"
	
	rotation = input_rotation
	
	position += movement * speed * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func _input(event):
	if([MODE.AI,MODE.NETWORK_SLAVE].has(current_mode)):
		return
	if Input.is_action_just_pressed("player_fire"):
		firing = true
	if Input.is_action_just_released("player_fire"):
		firing = false
	
	var temp_velocity = Vector2()
	if Input.is_action_pressed("player_right"):
	   temp_velocity.x += 1
	if Input.is_action_pressed("player_left"):
		temp_velocity.x -= 1
	if Input.is_action_pressed("player_down"):
		temp_velocity.y += 1
	if Input.is_action_pressed("player_up"):
		temp_velocity.y -= 1
	velocity = temp_velocity
	if Input.is_action_just_pressed("player_singlefire"):
		fire()

"""--- ship internal methods ---"""

func fire():
	if !can_fire || !$Cooldown.is_stopped():
		return
	if activebullets > maxammo - bullet_number:
		return
	$Cooldown.start()
	fire_projectile()
	
	if activebullets > maxammo - bullet_number and !reloading:
		reloading = true
		var oldest = get_oldest_bullet_life()
		if [MODE.LOCAL,MODE.NETWORK].has(current_mode):
			emit_signal("reloading",oldest)

func fire_mine():
	if !can_fire || activemines >= maxmine:
		return
	var m1 = Mine.instance()
	m1.position = position+Vector2(-30,0).rotated(rotation)
	m1.firedby = self.name
	m1.connect("expired", self,"mine_expired",[],CONNECT_ONESHOT)
	get_parent().add_child(m1)
	activemines += 1

func mine_expired():
	if activemines >= 1:
		activemines -= 1

func fire_projectile():
	#instance bullets
	var b1 = Bullet.instance()
	b1.connect("expired", self,"bullet_expired",[],CONNECT_ONESHOT)
	
	#set bullet properties
	b1.firedby = self.name
	b1.rotation = rotation
	b1.velocity = b1.velocity.rotated(rotation)
	b1.position = position+Vector2(26,0).rotated(rotation)
	
	#apply ship modifiers
	b1.maxlife = b1.maxlife*bulletlifemodifier
	b1.speed = b1.speed*bulletspeedmodifier
	b1.damage = b1.damage*bulletdamagemodifier
	var id = get_free_bullet_id()
	if id != -1:
		b1.id = id
		bullet_life_array[id] = bullet_duration
	#add to tree and detract ammo
	emit_signal("shoot",b1)
	activebullets += 1

func bullet_expired(id):
	if activebullets >= 1:
		activebullets -= 1
	if id != -1:
		bullet_life_array[id] = 0
	if activebullets <= maxammo - bullet_number:
		reloading = false
		if [MODE.LOCAL,MODE.NETWORK].has(current_mode):
			emit_signal("reloading",0)

func get_free_bullet_id():
	for i in range(maxammo):
		if bullet_life_array[i] <= 0:
			return i
	return -1

func get_oldest_bullet_life():
	var oldest = INF
	var candidate = null
	for i in range(maxammo):
		candidate = bullet_life_array[i]
		if candidate > 0 and candidate < oldest:
			oldest = candidate
	return oldest

func update_health_bar(new_hp):
	$HealthBar.value = round(new_hp)

func take_damage(damage_amount):
	health -= damage_amount
	emit_signal("health_changed",health)
	if health <= 0:
		end()

func _on_ship_entered(area):
	if area.is_in_group("Hitter"):
		var hitter = area
		var health_loss = area.damage
		if health_loss < 0:
			end()
			return
		if hitter.is_in_group("Bullet"):
			if hitter.firedby != $".".name:
				hitter.end()
				take_damage(health_loss)
