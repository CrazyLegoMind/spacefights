extends Node2D

class_name AsteroidClass

#per-asteroid stats
var max_speed = 350
var min_speed = 100
var max_scale = 100
var min_scale = 50
var max_health = 600
var point_bonus = 1

#asteroid-shared stats
var velocity = Vector2(1,0)
var screen_size
var damage = -1
var points:int
var health = max_health
var speed = 0
var dead = false
signal destroyed(pts,ignore,current_pos)

func _init(
	asteroid_max_speed,
	asteroid_min_speed,
	asteroid_max_scale,
	asteroid_min_scale,
	asteroid_max_health,
	asteroid_point_bonus
	):
	max_speed = asteroid_max_speed
	min_speed = asteroid_min_speed
	max_scale = asteroid_max_scale
	min_scale = asteroid_min_scale
	max_health = asteroid_max_health
	point_bonus = asteroid_point_bonus

func _ready():
	health = max_health * scale.x
	screen_size = get_viewport_rect().size

func _physics_process(delta):
	velocity = velocity.normalized() * speed
	position += velocity * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func end(destroyed = true):
	if dead:
		return
	dead = true
	if destroyed:
		emit_signal("destroyed",points,false,global_position)
	$Sprite.hide()
	$CollisionShape2D.call_deferred("set_disabled", true)
	$AnimatedSprite.show()
	$AnimatedSprite.play("destroyed")
	$Particles2D.emitting = true
	yield( get_node("AnimatedSprite"), "animation_finished" )
	$".".queue_free()

func update_points():
	var temp:float = (speed/max_speed*400) + ((scale.x*100)/max_scale)*200
	temp = temp * point_bonus
	points = int(temp)
	#print("pt: ",points," bn: ",point_bonus," sp: ",round((speed/max_speed)*100)," sc: ",round(((scale.x*100)/max_scale)*100))

func take_damage(damage_amount):
	health -= damage_amount
	if health <= 0:
		end()

func _on_Asteroid_area_entered(area):
	if area.is_in_group("Hitter") and !area.is_in_group("Asteroid"):
		var hitter = area
		var health_loss = area.damage
		if health_loss < 0:
			end()
			return
		if hitter.is_in_group("Bullet"):
			hitter.end()
			take_damage(health_loss)
			if health <= 0:
				return
			var x = range_lerp(health,0,max_health,0.3,1)
			modulate = Color(x,x,x,1)
