extends Node

const DEFAULT_IP = "127.0.0.1"
const DEFAULT_PORT = 25565
const MAX_PLAYERS = 5

var players = { }
var self_data = {
	name = '',
	position = Vector2(360, 180),
	ship = 0
}

signal player_registered(player_id,player_info)

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func create_server(player_name,player_ship):
	self_data.name = player_name
	self_data.ship = player_ship
	players[1] = self_data
	var peer = NetworkedMultiplayerENet.new()
	var res = peer.create_server(DEFAULT_PORT, MAX_PLAYERS)
	if res != OK:
		print_debug("[ERR] can't create a server, code: ",res)
	get_tree().set_network_peer(peer)


func connect_to_server(player_name,player_ship):
	self_data.name = player_name
	self_data.ship = player_ship
	#get_tree().connect('connected_to_server', self, '_connected_to_server') 
	#connessa a _connect_ok
	var peer = NetworkedMultiplayerENet.new()
	var res = peer.create_client(DEFAULT_IP, DEFAULT_PORT)
	if res != OK:
		print_debug("[ERR] can't connect to server, code: ",res)
	get_tree().set_network_peer(peer)


func _player_connected(id):
	print_debug("[INF] player connected ", id)
	var local_player_id = get_tree().get_network_unique_id()
	if not(get_tree().is_network_server()):
		rpc_id(1, 'request_player', local_player_id, id)


func _player_disconnected(id):
	players.erase(id)


func _connected_ok():
	var local_player_id = get_tree().get_network_unique_id()
	players[local_player_id] = self_data
	rpc('register_player', local_player_id, self_data)


func _server_disconnected():
	print_debug("[ERR] kicked by server")


func _connected_fail():
	print_debug("[ERR] connection failed")

remote func request_player(request_from_id, player_id):
	if get_tree().is_network_server():
		rpc_id(request_from_id, 'register_player', player_id, players[player_id])

remote func register_player(id,info):
	players[id] = info
	print_debug("[INF] asking for registration ", id)
	# Call function to update lobby UI here
	emit_signal("player_registered",id,info)
