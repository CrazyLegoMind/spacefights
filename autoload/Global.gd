extends Node

var pause_popup = preload("res://scenes/pausepopup/pausepopup.tscn")

var current_scene = null

var ai_level := 0
var player1_ship := 0
var player2_ship := 0
var display_ship := 0



var is_game_paused := false
var can_pause := false
var new_localbest := false
var previous_mouse_state
var pause_popup_instance


const ship_names := {
	0 : "The Square",
	1 : "Classic Beauty",
	2 : "Destroyer"
}
const SHIP_AMOUNT := 3

const background_names := {
	0: "default",
	1: "dark"
}
const BACKGROUND_AMOUNT := 2

var current_bg := 0
var background_texture = null

const pointermode_names := {
	0: "ship",
	1: "locked",
	2: "trickshot"
}
const POINTERMODE_AMOUNT := 3

var client_stats := {
	"SurvivalVers" : "654754688",
	"Player" : "default",
	"SurvivalRecords" : {
		#for each ship: "ship_number" : score
	},
	"LocalBest": {
		#"Record" : 0,
		#"Ship" : "0"
	},
	"LocalMode" : {
		"Unlocked" : 0,
		"Defeated" : [0,0,0]
	}
}

const SAVE_PATH := "user://config.cfg"
var _config_file := ConfigFile.new()
var _settings := {
	"Player" : {
		"Name" : "default",
		"Tutorial" : "todo",
		"Background" : 0,
		"PointerMode" : 0,
		"Fullscreen" : "off",
		"Sensitivity" : 100
	},
	"Data":{
		"Leaderboard" : []
	}
}


func _ready() -> void:
	#default generic actions
	randomize()
	var root := get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	self.set_pause_mode(2)
	set_process_input(true)
	#settings actions
	if load_settings() != 0:
		save_settings()
	apply_settings()


func goto_scene(path:String) -> void:
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path:String) ->void:
	current_scene.free()
	var next_scene := ResourceLoader.load(path)
	current_scene = next_scene.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)

func reload_scene() ->void:
		var to_reload:String = current_scene.filename
		goto_scene(to_reload)

func get_ship_name(ship:int) ->String:
	return ship_names[ship]

func ship_number_to_path(ship_number:int) -> String:
	var number_str := str(ship_number)
	return "res://scenes/ship"+number_str+"/Ship"+number_str+".tscn"

func get_background_name(background:int) -> String:
	return background_names[background]

func background_number_to_texture(bg_number:int) -> String:
	var bg_str := get_background_name(bg_number)
	return "res://assets/textures/Backgrounds/" +bg_str+".png"

func get_pointermode_name(mode:int) -> String:
	return pointermode_names[mode]

"""------------crypted data save section------------"""

func save_stats() -> void:
	var player_name:String = get_setting("Player","Name")
	if player_name == "default":
		return
	var file_path:String = "user://"+player_name+".save"
	var save_game := File.new()
	save_game.open_encrypted_with_pass(file_path, File.WRITE, OS.get_unique_id()+player_name)
	var player_data := client_stats
	save_game.store_line(to_json(player_data))
	save_game.close()

func load_stats() -> int:
	var player_name:String = get_setting("Player","Name")
	var file_path:String = "user://"+player_name+".save"
	var save_game := File.new()
	if not save_game.file_exists(file_path):
		return -1 # Error! We don't have a save to load.
	
	save_game.open_encrypted_with_pass(file_path, File.READ, OS.get_unique_id()+player_name)
	
	while not save_game.eof_reached():
		var current_line := save_game.get_line()
		if current_line == "":
			continue
		
		var current_line_dict:Dictionary = parse_json(current_line)
		if current_line_dict:
			for key in current_line_dict.keys():
				if key == "SurvivalVers":
					if str(current_line_dict[key])  != client_stats[key]:
						clear_records()
					continue
				if key in client_stats.keys():
					client_stats[key] = current_line_dict[key]
	save_game.close()
	return 0

func get_client_stat(category:String):
	return client_stats[category]

func set_client_stat(category:String, key:String, value):
	client_stats[category][key] = value

"""------------uncrypted config section------------"""

func save_settings() -> void:
	for section in _settings.keys():
		for key in _settings[section].keys():
			_config_file.set_value(section, key, _settings[section][key])
	_config_file.save(SAVE_PATH)

func load_settings() -> int:
	var error := _config_file.load(SAVE_PATH)
	if error != OK:
		return -1
	for section in _settings.keys():
		for key in _settings[section].keys():
			var val = _config_file.get_value(section,key)
			var valuetype := typeof(val)
			var settingstype := typeof(_settings[section][key])
			if valuetype == settingstype or (valuetype == TYPE_ARRAY and settingstype == TYPE_DICTIONARY):
				_settings[section][key] = val
			else:
				print_debug("a value was wrong in the cfg file: ", section," ", key,"->",typeof(val), "!=",typeof(_settings[section][key]))
	return 0

func get_setting(category:String, key):
	return _settings[category][key]

func set_setting(category:String, key:String, value):
	_settings[category][key] = value

func apply_settings() -> void:
		background_texture = load(Global.background_number_to_texture(_settings["Player"]["Background"]))
		if _settings["Player"]["Fullscreen"] == "on":
			OS.window_fullscreen = true

"""------------record managment section------------"""

func get_survival_record(ship) -> int:
	var record_dict = client_stats["SurvivalRecords"]
	if ship < SHIP_AMOUNT and ship >= 0:
		ship = str(ship)
		if ship in record_dict.keys():
			return record_dict[ship]
	return 0


func update_stats(score,ship):
	var record_dict = client_stats["SurvivalRecords"]
	if ship < SHIP_AMOUNT and ship >= 0:
		record_dict[str(ship)] = score
	check_new_localbest(score,ship)


func check_new_localbest(new_record_score,new_record_ship):
	var is_new = true
	for key in client_stats["SurvivalRecords"].keys():
		if client_stats["SurvivalRecords"][key] > new_record_score:
			is_new = false
	if is_new:
		client_stats["LocalBest"]["Ship"] = str(new_record_ship)
		client_stats["LocalBest"]["Record"] = new_record_score
		new_localbest = true

func get_localbest() -> Dictionary:
	if client_stats["LocalBest"].size() != 2:
		return {}
	var dict = {}
	dict[client_stats["Player"]] = client_stats["LocalBest"]
	return dict

func clear_records():
	client_stats["SurvivalRecords"] = {}
	client_stats["LocalBest"] = {}


"""------------general game management state and functions------------"""

func _input(event):
	if Input.is_action_just_pressed("ui_cancel") and (can_pause or is_game_paused):
		game_pause(!is_game_paused)

func game_pause(pause:bool):
	is_game_paused = pause
	if pause:
		previous_mouse_state = Input.get_mouse_mode()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		pause_popup_instance = pause_popup.instance()
		pause_popup_instance.connect("button_pressed",self,"popup_button_pressed")
		add_child(pause_popup_instance)
	else:
		Input.set_mouse_mode(previous_mouse_state)
		pause_popup_instance.queue_free()
	get_tree().paused = pause

func popup_button_pressed(button):
	if button == "tomenu":
		goto_scene("res://scenes/mainmenu/MainMenu.tscn")
		game_pause(false)
	if button == "restart":
		reload_scene()
		game_pause(false)
	if button == "continue":
		game_pause(false)

func set_pausable(pause:bool):
	can_pause = pause

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		save_stats()
		save_settings()
		get_tree().quit() # default behavior
