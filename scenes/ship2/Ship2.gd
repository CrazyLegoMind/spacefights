extends ShipClass

export var ship2_speed = 240
export var ship2_maxammo = 2
export var ship2_bulletspeedmodifier = 0.9
export var ship2_bulletlifemodifier = 1.1
export var ship2_bulletdamagemodifier = 1.5
export var ship2_maxhealth = 1200
export var ship2_description = "At the cost of low speed and reduced ammo, The Drestoyer has incredible bullet damage TO DESTROY EVERYTHING"
export var ship2_prj_spawn_x = 36
export var ship2_bullet_number = 1

func _init().(
		ship2_speed,
		ship2_maxammo,
		ship2_bulletspeedmodifier,
		ship2_bulletlifemodifier,
		ship2_bulletdamagemodifier,
		ship2_maxhealth,
		ship2_description,
		ship2_prj_spawn_x,
		ship2_bullet_number
	):
	pass

func fire_projectile():
	#instance bullets 
	var b1 = Bullet.instance()
	b1.connect("expired", self,"bullet_expired",[],CONNECT_ONESHOT)
	
	#set bullet properties
	b1.firedby = $".".name
	b1.rotation = rotation
	b1.velocity = b1.velocity.rotated(rotation)
	b1.position = position+Vector2(prj_spawn_x,0).rotated(rotation)
	
	#apply ship modifiers
	b1.maxlife = b1.maxlife*bulletlifemodifier
	b1.speed = b1.speed*bulletspeedmodifier
	b1.damage = b1.damage*bulletdamagemodifier
	
	var id = get_free_bullet_id()
	if id != -1:
		b1.id = id
		bullet_life_array[id] = bullet_duration
	
	#add to tree and detract ammo
	emit_signal("shoot",b1)
	activebullets += 1

