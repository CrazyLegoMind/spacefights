extends Node


func _ready():
	$Background.texture = Global.background_texture


func _on_Host_pressed():
	$HUD/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer/Host.text = "Hosting..."
	Network.create_server(Global.get_client_stat("Player"),Global.player1_ship)
	_load_game()


func _on_Connect_pressed():
	$HUD/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer/Connect.text = "Connecting..."
	Network.connect_to_server(Global.get_client_stat("Player"), Global.player1_ship)
	_load_game()

func _load_game():
	Global.goto_scene("res://scenes/multiplayer/Multiplayer.tscn")
