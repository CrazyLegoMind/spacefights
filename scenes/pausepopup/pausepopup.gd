extends Node

signal button_pressed(button)

func _ready():
	pass # Replace with function body.



func _on_ToMenu_pressed():
	emit_signal("button_pressed","tomenu")


func _on_Restart_pressed():
	emit_signal("button_pressed","restart")


func _on_Continue_pressed():
	emit_signal("button_pressed","continue")
