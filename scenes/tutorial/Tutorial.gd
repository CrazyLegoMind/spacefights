extends Node2D

export var hint_color: = Color("FFFF33")
export var hint_alpha: = 0.3
var player_node = null
var screen_size
enum STEP {
	INTRO,
	MOVE,
	SHOOT_1,
	SHOOT_2,
	ACCELERATION,
	END
}
var current_step = STEP.INTRO

onready var hint_node = get_node("CanvasLayer/MarginContainer/VBoxContainer/TutorialText")
var step_texts = [
	"""
	Welcome to SpaceFights! this is a brief tutorial to introduce the basic game mechanics. Confirm each step by cliking 'OK!' down here, or go to main menu with 'Skip tutorial', have fun!
	""",
	"first of all: controls, you move with WASD and aim with your mouse moovement, now try to reach that yellow spot",
	"you have two fire button here, LMB to fire a single projectile, RMB to fire as much as you can within the ship limits",
	"step 3",
	"step 4",
	"step 5",
]

func _draw():
	hint_color.a = hint_alpha
	match current_step:
		STEP.INTRO:
			pass
		STEP.MOVE:
			draw_circle($MoveArea.position,50,hint_color)
		STEP.SHOOT_1:
			pass
		STEP.SHOOT_2:
			pass
		STEP.ACCELERATION:
			pass
		STEP.END:
			pass


func _ready():
	$BackGround.texture = Global.background_texture
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$"/root/Global".set_pausable(true)
	screen_size = get_viewport().size
	spawn_player($Position2D.position)
	update_tutorial(false)

func update_tutorial(done_input):
	if done_input:
		current_step = current_step+1
	hint_node.text = step_texts[current_step]
	match current_step:
		STEP.INTRO:
			player_node.freeze(true)
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		STEP.MOVE:
			player_node.freeze(false)
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		STEP.SHOOT_1:
			pass
		STEP.SHOOT_2:
			pass
		STEP.ACCELERATION:
			pass
		STEP.END:
			pass
	update() 

func spawn_player(spawn_location):
	player_node = load($"/root/Global".ship_number_to_path(0)).instance()
	add_child(player_node)
	#player_node.connect("destroyed", self,"spawn_player")
	player_node.connect("shoot", self,"spawn_bullet")
	player_node.start(spawn_location)

func spawn_bullet(bullet):
	#bullet.connect("hit", self,"on_bullet_hit",[],CONNECT_ONESHOT)
	add_child(bullet)

func _on_Skip_pressed():
	Global.goto_scene("res://scenes/mainmenu/MainMenu.tscn")


func _on_Done_pressed():
	update_tutorial(true)


func _on_MoveArea_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("Ship"):
		match current_step:
			STEP.INTRO:
				pass
			STEP.MOVE:
				current_step = STEP.SHOOT_1
				update_tutorial(false)
			STEP.SHOOT_1:
				pass
			STEP.SHOOT_2:
				pass
			STEP.ACCELERATION:
				pass
			STEP.END:
				pass
