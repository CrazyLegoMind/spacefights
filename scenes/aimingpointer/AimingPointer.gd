extends Node2D

var too_away = false
var disable_color = Color(1,1,1,0.3)
var enable_color = Color(1,0,0,1)
var reload_color = Color(0.8,0,0,1)
var reload_delay = 0
var elapsing_delay = 0
var reloading = false
var screen_size = Vector2()
var mode = ""
var ship_position = Vector2(0,0)
var aiming_position = Vector2(0,0)


enum SIDE {UP,RIGHT,DOWN,LEFT,NONE}
var deltapos_array
var trickshot_trigger_side = SIDE.NONE
var movement_out_side = SIDE.NONE
var trickshot_state = false
var trickshot_vector = Vector2(0,0)
var ship_position_delta = Vector2(0,0)
var sensitivity = 100

func _ready():
	self.set_as_toplevel(true)
	screen_size = get_viewport_rect().size
	sensitivity = Global.get_setting("Player","Sensitivity")
	position = Vector2(screen_size.x/2,screen_size.y/2)
	aiming_position = position
	deltapos_array =[
		Vector2(0,screen_size.y),#UP
		Vector2(-screen_size.x,0),#RIGHT
		Vector2(0,-screen_size.y),#DOWN
		Vector2(screen_size.x,0),#LEFT
		Vector2(0,0)
	]
	mode = Global.get_setting("Player","PointerMode")


func reset():
	position = Vector2(screen_size.x/2,screen_size.y/2)
	trickshot_state = false
	ship_position_delta = deltapos_array[4]

func freeze(state:bool):
	set_process(!state)
	set_physics_process(!state)
	set_process_input(!state)

func _process(delta):
	if reloading:
		elapsing_delay -= delta
		$ReloadProgress.value = elapsing_delay/reload_delay*100
		if elapsing_delay <= 0:
			set_reloading(0)
	var rotation_vect = ship_position+ ship_position_delta - position
	if trickshot_state:
		$Arrow.play("blink")
		$Arrow.rotation = rotation_vect.angle()
	else:
		$Arrow.stop()
		$Arrow.frame = 0

func _input(event):
	if event is InputEventMouseMotion:
		update_pos(event.relative)

func update_pos(movement):
	var temp = movement
	movement *= sensitivity/100.0
	var new_pos = position + movement
	if mode == 0:
		new_pos.x = wrapf(new_pos.x, 0, screen_size.x)
		new_pos.y = wrapf(new_pos.y, 0, screen_size.y)
		aiming_position = new_pos
	elif mode == 1:
		new_pos.x = clamp(new_pos.x, 0, screen_size.x)
		new_pos.y = clamp(new_pos.y, 0, screen_size.y)
		aiming_position = new_pos
	elif mode == 2:
		if new_pos.x >= screen_size.x:
			movement_out_side = SIDE.RIGHT
		elif new_pos.y >= screen_size.y:
			movement_out_side = SIDE.DOWN
		elif new_pos.x < 0:
			movement_out_side = SIDE.LEFT
		elif new_pos.y < 0:
			movement_out_side = SIDE.UP
		else:
			movement_out_side = SIDE.NONE
		new_pos.x = wrapf(new_pos.x, 0, screen_size.x)
		new_pos.y = wrapf(new_pos.y, 0, screen_size.y)
		
		if ([SIDE.UP,SIDE.RIGHT,SIDE.DOWN,SIDE.LEFT]).has(movement_out_side) and !trickshot_state:
			trickshot_state = true
			trickshot_trigger_side = movement_out_side
			ship_position_delta = deltapos_array[trickshot_trigger_side]
		elif trickshot_state and movement_out_side != SIDE.NONE:
			if (movement_out_side+2)%4 == trickshot_trigger_side :
				trickshot_state = false
				ship_position_delta = deltapos_array[4]
		
		if trickshot_state:
			aiming_position = position - deltapos_array[trickshot_trigger_side]
		else:
			aiming_position = new_pos
		
	position = new_pos

func set_away(value:bool):
	too_away = value
	if too_away:
		self.modulate = disable_color
	else:
		self.modulate = enable_color

func set_reloading(delay):
	if delay == 0:
		$Sprite.show()
		$ReloadProgress.hide()
		reloading = false
	else:
		$ReloadProgress.show()
		$Sprite.hide()
		reload_delay = delay
		elapsing_delay = delay
		reloading = true
