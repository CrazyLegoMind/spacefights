extends Node

func _ready():
	$BackGround.texture = Global.background_texture
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Network.connect("player_registered",self,"spawn_player")
	spawn_player(get_tree().get_network_unique_id(),Network.self_data)



func spawn_player(id,info):
	var my_id = get_tree().get_network_unique_id()
	var new_player = load(Global.ship_number_to_path(info.ship)).instance()
	new_player.name = str(id)
	new_player.set_network_master(id)
	new_player.set_as_networked(id == my_id)
	
	get_tree().get_root().add_child(new_player)
	
	#new_player.connect("destroyed", self,"game_over")
	new_player.connect("shoot", self,"spawn_bullet")
	new_player.start(Vector2(0,0))

func spawn_bullet(bullet):
	add_child(bullet)

