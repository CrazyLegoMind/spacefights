extends AsteroidClass

var asteroid0_max_speed = 350
var asteroid0_min_speed = 100
var asteroid0_max_scale = 100
var asteroid0_min_scale = 50
var asteroid0_max_health = 600
var asteroid0_point_bonus = 1

func _init().(
	asteroid0_max_speed, 
	asteroid0_min_speed, 
	asteroid0_max_scale, 
	asteroid0_min_scale, 
	asteroid0_max_health,
	asteroid0_point_bonus
	):
	pass
