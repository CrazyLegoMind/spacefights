extends Area2D

export var speed = 600
export var maxlife = 1
export var damage = 400

var firedby = 0
var velocity = Vector2(1,0)
var currentlifetime
var screen_size
var expiring = false
var id = -1
signal expired(id)
signal hit

func _ready():
	screen_size = get_viewport_rect().size
	currentlifetime = 0
	$ShotSfx.pitch_scale = 0.9 + (randi()%10)/100.0
	$ShotSfx.play()

func _physics_process(delta):
	velocity = velocity.normalized() * speed
	position += velocity * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)
	currentlifetime += delta
	if currentlifetime >= maxlife:
		if(!expiring):
			end(false)

func end(destroyed = true):
	expiring = true
	emit_signal("expired",id)
	$CollisionShape2D.call_deferred("set_disabled", true)
	if destroyed:
		emit_signal("hit")
		velocity = Vector2()
		$Sprite.hide()
		$AnimatedSprite.show()
		$AnimatedSprite.play("destroyed")
		$HitSfx.play()
		yield( get_node("AnimatedSprite"), "animation_finished" )
		$ShotSfx.stop()
	queue_free()
