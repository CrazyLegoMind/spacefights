extends Node2D

export (PackedScene) var Asteroid
export (PackedScene) var SplittingAsteroid
export var round_max_asteroids = 3
export var splitting_asteroids_offset = 5

const score_row1_path = "HUD/ScoreContainer/Rows/Row1"
const score_row2_path = "HUD/ScoreContainer/Rows/Row2"
const score_row3_path = "HUD/ScoreContainer/Rows/Row3"
const fancy_score_path = "HUD/EndMessage/VBoxContainer/FancyStats"

enum ASTTYPE {NORMAL, SPLITTING}
var current_spawn_type = ASTTYPE.NORMAL

var combototal:float = 0
var highestcombostate = 0
var destroyedasteroids = 0

var current_asteroids = 0
var spawned_asteroids = 0
var screen_size
var player1_pos = Vector2()
var player1_vel
var ship_1
var player1_score:int = 0
var current_round = 1
var firedbullets =  0
var bullethits = 0
var accuracy:float = 0
var player_node = null
var combostate = 1
var combobonus = 100
signal countdown_end



func _ready():
	$BackGround.texture = Global.background_texture
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$"/root/Global".set_pausable(true)
	screen_size = get_viewport_rect().size
	player1_pos.x = screen_size.x/2
	player1_pos.y = screen_size.y/2
	spawn_player()
	player_node.set_process_input(false)
	$HUD/CounterContainer.hide()
	startgame()
	yield($".","countdown_end")
	player_node.set_process_input(true)

func spawn_player():
	player_node = load($"/root/Global".ship_number_to_path($"/root/Global".player1_ship)).instance()
	add_child(player_node)
	player_node.connect("destroyed", self,"game_over")
	player_node.connect("shoot", self,"spawn_bullet")
	player_node.start(player1_pos)
	player_node.rotation = -PI/2

func score_update(score):
	player1_score += int(score)
	update_HUD()

func game_over(player):
	$AsteroidTimer.stop()
	$HUD/CounterContainer.hide()
	get_node(score_row1_path +"/Score").hide()
	update_HUD()
	$HUD/EndMessage.show()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func startgame():
	$HUD/CounterContainer.show()
	get_node(score_row1_path +"/Asteroids").set_modulate(Color(1,1,1,1))
	update_HUD()
	var Counter = $HUD/CounterContainer/Counter
	$Timer.start()
	yield($Timer, "timeout")
	Counter.text = "2"
	yield($Timer, "timeout")
	Counter.text = "1"
	yield($Timer, "timeout")
	$Timer.stop()
	$HUD/CounterContainer.hide()
	Counter.text = "3"
	emit_signal("countdown_end")
	$AsteroidTimer.start()

func startround():
	get_node(score_row1_path +"/Asteroids").set_modulate(Color(1,1,1,1))
	update_HUD()
	spawned_asteroids = 0
	$AsteroidTimer.wait_time = 1
	$AsteroidTimer.start()

func spawn_bullet(bullet_node):
	firedbullets += 1
	accuracy = float(bullethits)/ firedbullets
	bullet_node.connect("hit", self,"on_bullet_hit",[],CONNECT_ONESHOT)
	add_child(bullet_node)
	update_HUD()

func spawn_asteroid(asteroid_node):
	asteroid_node.connect("destroyed",self,"asteroid_update",[],CONNECT_ONESHOT)
	current_asteroids += 1
	call_deferred("add_child",asteroid_node)
	update_HUD()

func on_bullet_hit():
	bullethits += 1
	accuracy = float(bullethits)/ firedbullets

func _on_AsteroidTimer_timeout():
	if current_asteroids >= round_max_asteroids:
		$AsteroidTimer.stop()
		$AnimationPlayer.play("max reached")
		get_node(score_row1_path +"/Asteroids").set_modulate(Color("7c476d"))
		return
	
	current_asteroids += 1
	spawned_asteroids += 1
	
	update_HUD()
	var asteroid = null
	if spawned_asteroids % splitting_asteroids_offset == 0:
		current_spawn_type = ASTTYPE.SPLITTING
		asteroid = SplittingAsteroid.instance()
		asteroid.connect("spawn_child",self,"spawn_asteroid",[])
	else:
		current_spawn_type = ASTTYPE.NORMAL
		asteroid = Asteroid.instance()
	
	asteroid.connect("destroyed",self,"asteroid_update",[],CONNECT_ONESHOT)
	
	$AsteroidPath/AsteroidSpawnLocation.set_offset(randi())
	var direction = $AsteroidPath/AsteroidSpawnLocation.rotation + PI / 2
	
	var asteroidscale = rand_range(asteroid.min_scale,asteroid.max_scale)/100
	var speed = rand_range(asteroid.min_speed,asteroid.max_speed)
	
	asteroid.position = $AsteroidPath/AsteroidSpawnLocation.position
	direction += rand_range(-PI / 4, PI / 4)
	asteroid.scale = Vector2(asteroidscale,asteroidscale)
	if current_spawn_type == ASTTYPE.NORMAL:
		asteroid.rotation = rand_range(direction, direction + PI/2)
	elif current_spawn_type == ASTTYPE.SPLITTING:
		asteroid.rotation = direction
	asteroid.speed = speed
	asteroid.velocity = asteroid.velocity.rotated(direction)
	asteroid.update_points()
	add_child(asteroid)
	
	if current_asteroids >= round_max_asteroids:
		$AsteroidTimer.stop()
		$AnimationPlayer.play("max reached")
		get_node(score_row1_path +"/Asteroids").set_modulate(Color("7c476d"))
	if spawned_asteroids >= round_max_asteroids:
		$AsteroidTimer.wait_time -= 0.2
		spawned_asteroids = 0

func asteroid_update(asteroid_points,ignore,destroy_pos):
	score_update(asteroid_points)
	destroyedasteroids += 1
	current_asteroids -= 1
	update_HUD()
	if $ComboCooldown.is_stopped():
		$ComboCooldown.start()
	else:
		var combo_label_node = $HUD/Combo
		combostate += 1
		if combostate > highestcombostate:
			highestcombostate = combostate
		score_update(combostate*combobonus)
		combototal += combostate*combobonus
		$ComboCooldown.start()
		var labelsize = combo_label_node.rect_size
		var spawnoffset = Vector2(labelsize.x/2,labelsize.y/2)
		combo_label_node.rect_position = destroy_pos-spawnoffset
		combo_label_node.text = "+"+str(combostate*combobonus)
		$ComboAnimation.stop()
		$ComboAnimation.play("ComboLabel")
		
	if current_asteroids <= 0 and $AsteroidTimer.is_stopped() and !ignore:
		round_max_asteroids +=1
		current_round += 1
		$AnimationPlayer.play("new round")
		yield(get_tree().create_timer(1.5), 'timeout')
		startround()

func update_HUD():
	get_node(score_row1_path +"/Asteroids").text = "Asteroids " + str(current_asteroids) + "/" + str(round_max_asteroids)
	get_node(score_row1_path +"/Round").text = "Round " + str(current_round)
	get_node(score_row1_path +"/Score").text = "Score " + str(player1_score)
	get_node(score_row2_path +"/Accuracy").text = "Hit rate " + str(round(accuracy*100)) + "%"
	get_node("HUD/EndMessage/VBoxContainer/Label").text = "FINAL SCORE: " + str(player1_score)
	get_node(fancy_score_path+"/Combo").text = "Higest combo: "+str(highestcombostate)+"x"
	get_node(fancy_score_path+"/Asteroids").text = "Destroyed asteroids: "+str(destroyedasteroids)
	if player1_score:
		get_node(fancy_score_path+"/PercentageBonus").text ="Combo score: "+str(round((combototal/player1_score)*100))+"%"
	else:
		get_node(fancy_score_path+"/PercentageBonus").text ="Combo score: 0%"

func quit_and_save():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var scored_with_ship = $"/root/Global".player1_ship
	var old_record = $"/root/Global".get_survival_record(scored_with_ship)
	if player1_score > old_record:
		$"/root/Global".update_stats(player1_score,scored_with_ship)
	$"/root/Global".goto_scene("res://scenes/mainmenu/MainMenu.tscn")

func restart_and_save():
	var scored_with_ship = $"/root/Global".player1_ship
	var old_record = $"/root/Global".get_survival_record(scored_with_ship)
	if player1_score > old_record:
		$"/root/Global".update_stats(player1_score,scored_with_ship)
	$"/root/Global".reload_scene()

func _on_ComboCooldown_timeout():
	combostate = 1
