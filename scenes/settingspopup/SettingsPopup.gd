extends MarginContainer

var texture

signal texture_change(texture)

func _ready():
	update_settings_btn()

"""----- SettingsMenu ----"""

func _on_BackgroundBtn_pressed():
	var current_bg = Global.get_setting("Player","Background")
	var new_bg = (current_bg+1) % Global.BACKGROUND_AMOUNT
	Global.set_setting("Player","Background",new_bg)
	Global.background_texture = load(Global.background_number_to_texture(Global.get_setting("Player","Background")))
	emit_signal("texture_change", Global.background_texture)
	update_settings_btn()

func _on_PointerBtn_pressed():
	var new_mode = (Global.get_setting("Player","PointerMode")+1)% Global.POINTERMODE_AMOUNT
	Global.set_setting("Player","PointerMode",new_mode)
	update_settings_btn()

func update_settings_btn():
	$"HBoxContainer/VBoxContainer/Row1/BackgroundBtn".text = Global.get_background_name(Global.get_setting("Player","Background"))
	$"HBoxContainer/VBoxContainer/Row2/PointerBtn".text = Global.get_pointermode_name(Global.get_setting("Player","PointerMode"))
	if OS.window_fullscreen:
		$"HBoxContainer/VBoxContainer/Row3/FullScreenBtn".text = "on"
	else:
		$"HBoxContainer/VBoxContainer/Row3/FullScreenBtn".text = "off"
	$"HBoxContainer/VBoxContainer/Row4/Sensitivity".value = Global.get_setting("Player","Sensitivity")

func _on_FullScreenBtn_pressed():
	var new_fs = "off"
	OS.window_fullscreen = !OS.window_fullscreen
	if OS.window_fullscreen:
		new_fs = "on"
	Global.set_setting("Player","Fullscreen",new_fs)
	update_settings_btn()

func _on_Sensitivity_value_changed(value):
		Global.set_setting("Player","Sensitivity",int(value))
