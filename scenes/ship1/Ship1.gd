extends ShipClass

export var ship1_speed = 400
export var ship1_maxammo = 4
export var ship1_bulletspeedmodifier = 1
export var ship1_bulletlifemodifier = 0.5
export var ship1_bulletdamagemodifier = 0.75
export var ship1_maxhealth = 1200
export var ship1_description = "A classic pointy ship, with a big old-fancy bonus: it shoots 2 projectiles at time! also its short range is compensated by a fast speed"
export var ship1_prj_spawn_x = 20
export var ship1_bullet_number = 2

func _init().(
		ship1_speed,
		ship1_maxammo,
		ship1_bulletspeedmodifier,
		ship1_bulletlifemodifier,
		ship1_bulletdamagemodifier,
		ship1_maxhealth,
		ship1_description,
		ship1_prj_spawn_x,
		ship1_bullet_number
	):
	pass

func fire_projectile():
	#instance bullets
	var b1 = Bullet.instance()
	var b2 = Bullet.instance()
	b1.connect("expired", self,"bullet_expired",[],CONNECT_ONESHOT)
	b2.connect("expired", self,"bullet_expired",[],CONNECT_ONESHOT)
	
	#set bullet properties
	b1.firedby = $".".name
	b2.firedby = $".".name
	b1.rotation = rotation
	b2.rotation = rotation
	b1.position = position+Vector2(prj_spawn_x,15).rotated(rotation)
	b2.position = position+Vector2(prj_spawn_x,-15).rotated(rotation)

	b1.velocity = b1.velocity.rotated(rotation)
	b2.velocity = b2.velocity.rotated(rotation)
	
	#apply ship modifiers
	b1.speed = b1.speed*bulletspeedmodifier
	b2.speed = b2.speed*bulletspeedmodifier
	b1.maxlife = b1.maxlife*bulletlifemodifier
	b2.maxlife = b2.maxlife*bulletlifemodifier
	b1.damage = b1.damage*bulletdamagemodifier
	b2.damage = b2.damage*bulletdamagemodifier
	
	var id = get_free_bullet_id()
	if id != -1:
		b1.id = id
		bullet_life_array[id] = bullet_duration
	id = get_free_bullet_id()
	if id != -1:
		b2.id = id
		bullet_life_array[id] = bullet_duration
	
	#add to tree and detract ammo
	emit_signal("shoot",b1)
	emit_signal("shoot",b2)
	activebullets += 2

