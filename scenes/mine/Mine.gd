extends Node2D

export var damage = 800
export var explosion_delay = 0.1
var firedby = 0
var exploding = false

signal expired

func _ready():
	$Mine/AnimatedSprite.play("idle")

func hit(damage,hitter):
	if hitter.is_in_group("Bullet"):
		hitter.end()
	explode()


func explode():
	if !exploding:
		exploding = true
	else: 
		return
	yield(get_tree().create_timer(explosion_delay), 'timeout')
	var targets = $Explosion.get_overlapping_areas()
	for target in targets:
		if target.is_in_group("Hittable") && target != $Mine:
			target.take_damage(damage)
	end()

func end(destroyed = true):
	$Mine/ContactCollision.call_deferred("set_disabled", true)
	$Explosion/ExplosionCollision.call_deferred("set_disabled", true)
	if destroyed:
		emit_signal("expired")
		$Mine/AnimatedSprite.z_index = 2
		$Mine/AnimatedSprite.play("explosion")
		yield($Mine/AnimatedSprite,"animation_finished")
	queue_free()

func take_damage(damage_amount):
	if damage_amount != 0:
		explode()

func _on_Mine_area_entered(area):
	if area.is_in_group("Hitter") and area.is_in_group("Bullet"):
		area.end()
		explode()

func _on_Explosion_area_entered(area):
	if !area.is_in_group("Bullet") and( area.is_in_group("Hitter") or area.is_in_group("Ship") ):
		if area.is_in_group("Ship") and firedby == area.name:
			return
		explode()
