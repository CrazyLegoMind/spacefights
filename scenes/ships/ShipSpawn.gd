extends CenterContainer

export var rad_per_sec = PI/5

var current_ship
var ship_displayed
var current_rotation = 0

func _process(delta):
	var ship_to_display = $"/root/Global".display_ship
	if current_ship != ship_to_display:
		current_ship = ship_to_display
		#ship_displayed = load($"/root/Global".ship_number_to_path(ship_to_display)).instance()
		$ShipSprite.texture = load("res://assets/ship_sprites/Ship"+str(ship_to_display)+"Idle_1.png")
		$ShipSprite.scale = Vector2(1.5,1.5)
	current_rotation += delta*rad_per_sec
	$ShipSprite.rotation = current_rotation
