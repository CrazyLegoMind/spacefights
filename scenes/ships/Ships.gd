extends Node

var ship_data_container_path = "TextureRect/HUD/MarginContainer/VSplitContainer/HSplitContainer/DataContainer/ShipData/Data"
var bullet_data_container_path = "TextureRect/HUD/MarginContainer/VSplitContainer/HSplitContainer/DataContainer/BulletData/Data"
var ship_container_path = "TextureRect/HUD/MarginContainer/VSplitContainer/HSplitContainer/ShipContainer"
var ship_descripton_path = "TextureRect/HUD/MarginContainer/VSplitContainer/DescriptionContainer/Description"
var bullet = preload("res://scenes/bullet/Bullet.tscn")
var bullet_speed
var bullet_life
var bullet_damage
var current_ship
var ship_number


func _ready():
	bullet = bullet.instance()
	bullet_speed = bullet.speed
	bullet_life = bullet.maxlife
	bullet_damage = bullet.damage
	bullet.free()
	
	$TextureRect.texture = Global.background_texture
	update_ship()

func update_ship():
	ship_number = $"/root/Global".display_ship
	current_ship = load($"/root/Global".ship_number_to_path(ship_number)).instance()
	get_node(ship_container_path + "/ShipName").text = $"/root/Global".ship_names[ship_number]
	
	
	get_node(ship_data_container_path + "/ShipHealth").text =str(current_ship.maxhealth)
	get_node(ship_data_container_path + "/ShipSpeed").text =str(current_ship.speed)
	get_node(ship_data_container_path + "/ShipAmmo").text =str(current_ship.maxammo)
	get_node(ship_data_container_path + "/MissDPS").text = str(round((current_ship.bulletdamagemodifier*bullet_damage*current_ship.maxammo)*(1/(current_ship.bulletlifemodifier*bullet_life))))
	get_node(ship_data_container_path + "/HitDPS").text = str(round(current_ship.bulletdamagemodifier*bullet_damage*current_ship.bullet_number*(1/0.15)))
	get_node(ship_data_container_path + "/Range").text = str(round((current_ship.bulletspeedmodifier*bullet_speed*current_ship.bulletlifemodifier*bullet_life)+current_ship.prj_spawn_x))
	
	
	get_node(bullet_data_container_path + "/BulletSpeed").text =str(round(current_ship.bulletspeedmodifier*bullet_speed))
	get_node(bullet_data_container_path + "/BulletLife").text =str(current_ship.bulletlifemodifier*bullet_life)+"s"
	get_node(bullet_data_container_path + "/BulletDamage").text =str(round(current_ship.bulletdamagemodifier*bullet_damage))
	
	get_node(ship_descripton_path).text = current_ship.description
	current_ship.queue_free()

func _input(event):
	if Input.is_action_pressed("ui_cancel"):
		$"/root/Global".goto_scene("res://scenes/mainmenu/MainMenu.tscn")

func _on_NextShip_pressed():
	var total_ships = $"/root/Global".SHIP_AMOUNT
	ship_number = ($"/root/Global".display_ship+1)%total_ships
	$"/root/Global".display_ship = ship_number
	update_ship()

func _on_PreviousShip_pressed():
	var total_ships = $"/root/Global".SHIP_AMOUNT
	ship_number = $"/root/Global".display_ship-1
	if ship_number <0:
		ship_number = total_ships-1
	$"/root/Global".display_ship = ship_number
	update_ship()

func _on_Set1_pressed():
	$"/root/Global".player1_ship = $"/root/Global".display_ship


func _on_Set2_pressed():
	$"/root/Global".player2_ship = $"/root/Global".display_ship
