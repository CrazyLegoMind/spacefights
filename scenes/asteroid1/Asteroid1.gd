extends AsteroidClass
var is_child = false
var child = load("res://scenes/asteroid1/Asteroid1.tscn")
var child1_node = null
var child2_node = null

signal spawn_child(child_node)

var asteroid1_max_speed = 200
var asteroid1_min_speed = 100
var asteroid1_max_scale = 100
var asteroid1_min_scale = 76
var asteroid1_max_health = 800
var asteroid1_point_bonus = 1.2

func _init().(
		asteroid1_max_speed,
		asteroid1_min_speed,
		asteroid1_max_scale,
		asteroid1_min_scale,
		asteroid1_max_health,
		asteroid1_point_bonus
	):
	pass

func _ready():
	health = max_health * scale.x
	screen_size = get_viewport_rect().size

func _physics_process(delta):
	if is_child:
		rotation += PI/2*delta
	velocity = velocity.normalized() * speed
	position += velocity * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)

func end(destroyed = true):
	if dead:
		return
	dead = true
	if destroyed:
		emit_signal("destroyed",points,!is_child,global_position)
	$Sprite.hide()
	$CollisionShape2D.call_deferred("set_disabled",true)
	$Particles2D.emitting = true
	spawn_childs()
	yield(get_tree().create_timer(0.6),"timeout")
	queue_free()

func spawn_childs():
	if is_child:
		return
	child1_node = child.instance()
	child2_node = child.instance()
	child1_node.is_child = true
	child2_node.is_child = true
	child1_node.scale = scale*0.68
	child2_node.scale = scale*0.68
	child1_node.position = $Child1Pos.global_position
	child2_node.position = $Child2Pos.global_position
	child1_node.speed = speed/1.5
	child2_node.speed = speed/1.5
	child1_node.velocity = Vector2(-velocity.y,velocity.x)
	child2_node.velocity = Vector2(velocity.y,-velocity.x)
	child1_node.point_bonus = 0.9
	child2_node.point_bonus = 0.9
	child1_node.update_points()
	child2_node.update_points()
	emit_signal("spawn_child",child1_node)
	emit_signal("spawn_child",child2_node)

