extends CenterContainer

var total_ships

func _ready():
	total_ships = $"/root/Global".SHIP_AMOUNT
	var ship_number = randi()%total_ships
	$Sprite.texture = load("res://assets/ship_sprites/Ship"+str(ship_number)+"Idle_1.png")
	$Sprite.scale = Vector2(1.5,1.5)

func _process(delta):
	$Sprite.rotation = get_local_mouse_position().angle() +PI/2
