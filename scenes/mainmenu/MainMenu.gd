extends TextureRect



const AI_MAX_IMPLEMENTED := 2
const PATH_LOCAL_SHIP_BTN := "MainMenu/HBoxContainer/VBoxContainer/MenuOptions/PlayLocalContainer/HBoxContainer"
const PATH_SURVIVAL_SHIP_BTN := "MainMenu/HBoxContainer/VBoxContainer/MenuOptions/PlaySurvivalContainer/HBoxContainer"
const PATH_LEADERBOARD_LABEL := "MainMenu/HBoxContainer/ShipAndStats/OnlineContainer/LeaderboardScroll/Label"
const PATH_LEADERBOARD_BTNS_CONTAINER := "MainMenu/HBoxContainer/ShipAndStats/OnlineContainer/HBoxContainer"
const PATH_LOCAL_AI_BTN := "MainMenu/HBoxContainer/VBoxContainer/MenuOptions/PlayLocalContainer/AiBtnContainer/AiLevel"
const HARDNESS := ["easy","normal","hard","hardcore","ragequit"]

var _current_ai_level := 0

func _ready() -> void:
	Global.set_pausable(false)
	texture = Global.background_texture
	var leaderboard_stored:Array = Global.get_setting("Data","Leaderboard")
	if typeof(leaderboard_stored) == TYPE_ARRAY and leaderboard_stored.size() > 0:
		update_leaderboard_label(leaderboard_stored)
	update_ship_btn()
	_current_ai_level = Global.ai_level
	get_node(PATH_LOCAL_AI_BTN).text = HARDNESS[_current_ai_level]
	if Global.new_localbest:
		_on_RecordSend_pressed()
		Global.new_localbest = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _input(event):
	if Input.is_action_pressed("ui_cancel"):
		$MainMenu.show()
		$OptionsText.hide()
		$SettingsMenu.hide()


func update_ship_btn() -> void:
	var ship_dict:Dictionary = Global.ship_names
	var player1_ship_name:String = ship_dict[Global.player1_ship]
	var player2_ship_name:String = ship_dict[Global.player2_ship]
	get_node(PATH_LOCAL_SHIP_BTN+ "/player1ship").text = player1_ship_name
	get_node(PATH_LOCAL_SHIP_BTN+ "/player2ship").text = player2_ship_name
	get_node(PATH_SURVIVAL_SHIP_BTN+ "/player1ship").text = player1_ship_name


"""----- Mainmenu ----"""

func _on_Play_pressed() -> void:
	Global.goto_scene("res://scenes/lobby/Lobby.tscn")


func _on_PlayLocal_pressed() -> void:
	Global.goto_scene("res://scenes/localplay/localplay.tscn")


func _on_Ships_pressed() -> void:
	Global.goto_scene("res://scenes/ships/Ships.tscn")


func _on_player1ship_pressed() -> void:
	Global.player1_ship = (Global.player1_ship + 1)%Global.SHIP_AMOUNT
	update_ship_btn()


func _on_player2ship_pressed() -> void:
	Global.player2_ship = (Global.player2_ship + 1)%Global.SHIP_AMOUNT
	update_ship_btn()


func _on_PlaySurvival_pressed() -> void:
	Global.goto_scene("res://scenes/survivalplay/survivalplay.tscn")


func _on_Guide_pressed() -> void:
	$MainMenu.hide()
	$OptionsText.show()


func _on_Settings_pressed() -> void:
	$MainMenu.hide()
	$SettingsMenu.show()


func update_leaderboard_label(leaderboard_array:Array) -> void:
	#print_debug(leaderboard_array)
	if leaderboard_array.size() == 0:
		get_node(PATH_LEADERBOARD_LABEL).text = "Empty leaderboard"
		return
	
	var label_text := ""
	var ship_name := ""
	var str_record := ""
	var str_ship := ""
	
	for i in range(leaderboard_array.size()-1,-1,-1):
		ship_name = leaderboard_array[i]["name"]
		str_record = str(leaderboard_array[i]["score"])
		str_ship = str(leaderboard_array[i]["text"]).percent_decode()
		label_text = ship_name +":  "+str_record+ " - "+str_ship +"\n" + label_text
	
	Global.set_setting("Data","Leaderboard",leaderboard_array)
	Global.save_settings()
	get_node(PATH_LEADERBOARD_LABEL).text = label_text


func _on_RecordSend_pressed() -> void:
	var btn:Button = get_node(PATH_LEADERBOARD_BTNS_CONTAINER+"/RecordSend")
	btn.disabled = true
	get_node(PATH_LEADERBOARD_LABEL).text = "Loading..."
	$HTTPOnlineRecords.send_localbest()
	var err:int = yield($HTTPOnlineRecords,"localbest_sent")
	#print_debug(err)
	if err == 0:
		update_leaderboard_label($HTTPOnlineRecords.get_response_leaderboard())
	elif err == 1:
		get_node(PATH_LEADERBOARD_LABEL).text = "you have no local best"
		yield(get_tree().create_timer(2), 'timeout')
		update_leaderboard_label(Global.get_setting("Data","Leaderboard"))
	elif err == 2:
		get_node(PATH_LEADERBOARD_LABEL).text = "ERROR:\n  client version mismatch\n  try update the client"
		yield(get_tree().create_timer(2), 'timeout')
		update_leaderboard_label(Global.get_setting("Data","Leaderboard"))
	elif err == 3:
		get_node(PATH_LEADERBOARD_LABEL).text = "No need to update\nold record is higher"
		yield(get_tree().create_timer(2), 'timeout')
		update_leaderboard_label($HTTPOnlineRecords.get_response_leaderboard())
	else:
		get_node(PATH_LEADERBOARD_LABEL).text = "unexpected error: "+ str(err)
		yield(get_tree().create_timer(2), 'timeout')
		update_leaderboard_label(Global.get_setting("Data","Leaderboard"))
	btn.disabled = false


func _on_UpdateLeaderboard_pressed():
	var btn:Button = get_node(PATH_LEADERBOARD_BTNS_CONTAINER+"/UpdateLeaderboard")
	btn.disabled = true
	get_node(PATH_LEADERBOARD_LABEL).text = "Loading..."
	$HTTPOnlineRecords.request_get_records()
	yield($HTTPOnlineRecords,"request_get_completed")
	update_leaderboard_label($HTTPOnlineRecords.get_response_leaderboard())
	btn.disabled = false


func _on_AiLevel_pressed():
	var localmode_dict:Dictionary = Global.get_client_stat("LocalMode")
	var ai_btn_node:Button = get_node(PATH_LOCAL_AI_BTN)
	if _current_ai_level + 1 > localmode_dict["Unlocked"]:
		if _current_ai_level +1 >= HARDNESS.size():
			ai_btn_node.text = "do you really want more??"
		else:
			ai_btn_node.text = "you haven't unlocked the next difficulty yet"
		yield(get_tree().create_timer(1), 'timeout')
		ai_btn_node.text = HARDNESS[0]
		_current_ai_level = 0
	else:
		if _current_ai_level +1 > AI_MAX_IMPLEMENTED:
			ai_btn_node.text = "harder AIs under contruction, don't worry"
			yield(get_tree().create_timer(1), 'timeout')
			ai_btn_node.text = HARDNESS[0]
			_current_ai_level = 0
		else:
			_current_ai_level += 1
			ai_btn_node.text = HARDNESS[_current_ai_level]
	Global.ai_level = _current_ai_level


"""----- OptionsText ----"""

func _on_DiscordInvite_pressed():
	OS.shell_open("https://discordapp.com/invite/RSvy648")


func _on_GoogleForm_pressed():
	OS.shell_open("https://forms.gle/xHqNu1vANFK2RGQn6")


func _on_Exit_pressed():
	Global._notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)


func _on_SettingsMenu_texture_change(new_texture):
	texture = new_texture
