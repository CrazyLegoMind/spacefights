#best practices WIP
extends HTTPRequest

signal response_processed
signal localbest_sent(retvalue)
signal request_get_completed
signal request_send_completed

const DREAMLO_URL := "http://dreamlo.com/lb/"
var DREAMLO_PRIVATE := "mIn8Rl5wCkCStEyIK1O3OgMtbX5J2_DEG7ptQaRpmo9A"
const DREAMLO_PUBLIC := "5d0e5c3620139908f87eb7ae"

var parsed_response_raw := {}
var parsed_response_leaderboard := {} 
var current_player:String
var _requesting := false


func _ready():
	current_player = Global.get_client_stat("Player")


#function to be called from outside when we want to access the online leaderboard through
#get_response_leaderboard after sending the local best
func send_localbest() -> void:
	yield(get_tree().create_timer(1), 'timeout')
	var new_record_dict: = Global.get_localbest()
	if new_record_dict.size() != 1:
		emit_signal("localbest_sent",1)
		print_debug("malformed localbest", new_record_dict)
		return
	request_get_records()
	yield(self,"request_get_completed")
	var current_vers:String = Global.get_client_stat("SurvivalVers")
	if current_vers != get_Survival_online_version():
		emit_signal("localbest_sent",2)
		print_debug("version mismatch on send_localbest()")
		return
	request_send_record(new_record_dict)
	yield(self,"request_send_completed")
	request_get_records()
	yield(self,"request_get_completed")
	emit_signal("localbest_sent",0)


#issue a get request to the parsed response variables
func request_get_records() -> void:
	if _requesting:
		return
	else:
		_requesting = true
	var use_ssl := false
	var headers := ["User-Agent: SpaceFights (Godot)","Accept: */*"]
	var url := DREAMLO_URL + DREAMLO_PUBLIC + "/json"
	request(url, headers, use_ssl, HTTPClient.METHOD_GET)
	yield(self,"response_processed")
	_requesting = false
	parsed_response_leaderboard = parsed_response_raw
	emit_signal("request_get_completed")


#issue a send request to add a record to the online leaderboard
func request_send_record(record_dict) -> void:
	if _requesting:
		return
	else:
		_requesting = true
	var use_ssl := false
	var headers := ["User-Agent: SpaceFights (Godot)","Accept: */*"]
	var url:String = DREAMLO_URL + DREAMLO_PRIVATE +"/add"+get_record_string(record_dict)
	request(url, headers, use_ssl, HTTPClient.METHOD_GET)
	yield(self,"response_processed")
	_requesting = false
	emit_signal("request_send_completed")


#porcess a response, happens every time a request it's issued
#updates parsed_response_raw to the raw response
func _on_HTTPOnlineRecords_request_completed(result, response_code, headers, body) -> void:
	var bodystr:String = body.get_string_from_utf8()
	var jsonresult:JSONParseResult
	parsed_response_raw = {}
	if bodystr == "OK":
		pass
	else:
		jsonresult = JSON.parse(bodystr)
		if jsonresult.error == OK:
			parsed_response_raw = jsonresult.result
		else: 
			print_debug("error paring the HTTP leaderboard result", jsonresult.error)
	
	#print_debug(body.get_string_from_utf8())
	#print_debug(parsed_response_raw)
	emit_signal("response_processed")


func get_record_string(record_dict:Dictionary) -> String:
	var shipstr := str(Global.get_ship_name(int(record_dict[current_player]["Ship"])))
	shipstr = shipstr.percent_encode()
	var result:String = "/"+current_player+"/" + str(record_dict[current_player]["Record"]) +"/0/"+shipstr
	return result


#get the leaderboard from the updated online dict
func get_response_leaderboard() -> Array:
	if parsed_response_leaderboard.size() == 0 or typeof(parsed_response_leaderboard) != TYPE_DICTIONARY:
		print_debug("no copy of online dict: ",parsed_response_leaderboard)
		return []
	
	var entries = parsed_response_leaderboard["dreamlo"]["leaderboard"]["entry"]
	var leaderboard_array := []
	
	if typeof(entries) == TYPE_ARRAY:
		if entries[0]["name"] == "SurvivalVers":
			leaderboard_array = entries
			leaderboard_array.remove(0)
	elif typeof(entries) == TYPE_DICTIONARY:
		if entries["name"] == "SurvivalVers":
			leaderboard_array = ["Empty leaderboard, be the first!"]
	else:
		print_debug("malformed online leaderboard, ask the dev")
		return []
	return leaderboard_array


func get_Survival_online_version() -> String:
	if parsed_response_leaderboard.size() == 0 or typeof(parsed_response_leaderboard) != TYPE_DICTIONARY:
		print_debug("no copy of online dict",parsed_response_leaderboard)
		return ""
	
	var entries = parsed_response_leaderboard["dreamlo"]["leaderboard"]["entry"]
	
	if typeof(entries) == TYPE_ARRAY:
		if entries[0]["name"] == "SurvivalVers":
			return entries[0]["text"]
		else:
			print_debug("version backup not in 0 position on the leaderboard")
	elif typeof(entries) == TYPE_DICTIONARY:
		if entries["name"] == "SurvivalVers":
			return entries["text"]
		else:
			print_debug("version backup not in 0 position on the leaderboard")
	else:
		print_debug("malformed online leaderboard, ask the dev")
	return ""


func update_leaderboard_cfg(new_leaderboard:Dictionary) -> void:
	Global.set_setting("Data","Leaderboard",new_leaderboard)
