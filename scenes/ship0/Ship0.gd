extends ShipClass

export var ship0_speed = 300
export var ship0_maxammo = 3
export var ship0_bulletspeedmodifier = 1
export var ship0_bulletlifemodifier = 1
export var ship0_bulletdamagemodifier = 1
export var ship0_maxhealth = 1200
export var ship0_description = "In the beginning there was a square... and it still here! This is the basiest ship with the basiest stats"
export var ship0_prj_spawn_x = 26
export var ship0_bullet_number = 1

func _init().(
		ship0_speed,
		ship0_maxammo,
		ship0_bulletspeedmodifier,
		ship0_bulletlifemodifier,
		ship0_bulletdamagemodifier,
		ship0_maxhealth,
		ship0_description,
		ship0_prj_spawn_x,
		ship0_bullet_number
	):
	pass

func fire_projectile():
	#instance bullets 
	var b1 = Bullet.instance()
	b1.connect("expired", self,"bullet_expired",[],CONNECT_ONESHOT)
	
	#set bullet properties
	b1.firedby = self.name
	b1.rotation = rotation
	b1.velocity = b1.velocity.rotated(rotation)
	b1.position = position+Vector2(prj_spawn_x,0).rotated(rotation)
	
	#apply ship modifiers
	b1.maxlife = b1.maxlife*bulletlifemodifier
	b1.speed = b1.speed*bulletspeedmodifier
	b1.damage = b1.damage*bulletdamagemodifier
	var id = get_free_bullet_id()
	if id != -1:
		b1.id = id
		bullet_life_array[id] = bullet_duration
	#add to tree and detract ammo
	activebullets += 1
	emit_signal("shoot",b1)


