extends Node2D


func _ready() -> void:
	
	$BackGround.texture = Global.background_texture
	var current_player : String = Global.get_setting("Player","Name")
	if current_player != "default":
		$NameField.placeholder_text = "Player: " + current_player
		$NameField.hide()
		$Button.disabled = false


func _on_Button_pressed() -> void:
	Global.load_stats()
	"""
	var tutorial_state = Global.get_setting("Player","Tutorial")
	if tutorial_state == "todo":
		Global.goto_scene("res://scenes/tutorial/Tutorial.tscn")
	elif tutorial_state == "done":
		Global.goto_scene("res://scenes/mainmenu/MainMenu.tscn")
	"""
	Global.goto_scene("res://scenes/mainmenu/MainMenu.tscn")

func _on_NameField_text_entered(new_text:String) -> void:
	Global.client_stats["Player"] = new_text
	Global.set_setting("Player","Name",new_text)
	Global.save_settings()
	$Button.disabled = false
