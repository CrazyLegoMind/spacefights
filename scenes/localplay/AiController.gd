extends Node

var Player1 = null
var Player2 = null
var aifirerange
var targetfirerange
var targetmaxspeed 
var aibulletspeed
var randangle = 0
var move_like_crazy = false
var ai_timer = 0
var AI_CHECK_INTERVAL = 0.05

#modes
enum HARDNESS {
	EASY = 0,
	NORMAL = 1,
	HARD = 2,
	HARDCORE = 3,
	RAGEQUIT = 4,
	NONE = -1
}
var mode = 1
var active = false

#easy difficulty variables
var targetPos = Vector2(0,0)
var aiPos  = Vector2(0,0)
var distance_vector = Vector2(0,0)
var distance = 0
var distance_norm = Vector2(0,0)
var velocity = Vector2(0,0)

#medium difficulty variables
var targetdir = Vector2(0,0)
var targetspeed_vect = Vector2(0,0)

#hard difficulty variables
var delta_cords_array = []
var screen_size = Vector2()

func _ready():
	screen_size = get_viewport().size
	populate_cords_array(screen_size)

func initialize_ai_data():
	aifirerange = Player2.firerange
	targetfirerange = Player1.firerange
	aibulletspeed = Player2.bulletspeedmodifier*600
	targetmaxspeed = Player1.speed

func set_active(value:bool):
	active = value

func set_ai_level(value):
	mode = value

func set_players_node(player1_node,player2_node):
	Player1 = player1_node
	Player2 = player2_node

"""----- general ai code and functions -----"""
func _on_Random_timeout():
	var delay_time = (randi()%100+1)/100.0
	$Random.wait_time = delay_time
	$Random.start()
	var mov_type = randf()*100
	if mov_type < 10 : #charge at the opponent 10% chance
		randangle = 0
	elif mov_type < 55  : #move right 45% chance
		randangle = rand_range(PI/4,3*(PI/4))
	else: #move left 45% chance 
		randangle = rand_range(-PI/4,-3*(PI/4))
	$RandomEnd.start()

func _on_RandomEnd_timeout():
	randangle = 0

func _process(delta):
	
	
	if(!active):
		if !$Random.is_stopped():
			$Random.stop()
		return
	elif $Random.is_stopped():
		$Random.start()
	
	ai_timer += delta
	if ai_timer < AI_CHECK_INTERVAL:
		return
	else:
		ai_timer = 0
	
	if mode == HARDNESS.EASY:
		easy_ai_behaviour()
	elif mode == HARDNESS.NORMAL:
		medium_ai_behaviour()
	elif mode == HARDNESS.HARD:
		hard_ai_behaviour()
	elif mode == HARDNESS.NONE:
		pass
	else:
		print_debug("[ERROR] mode not implemented: ",mode)

"""----- EASY ai code and functions -----"""

func easy_ai_behaviour():
	#variables
	targetPos = Player1.position
	aiPos = Player2.position
	distance_vector = targetPos - aiPos
	distance = distance_vector.length()
	velocity = Vector2(0,0)
	
	#angle
	Player2.rotation = distance_vector.angle()
	
	#velocity
	if distance > aifirerange:
		velocity = distance_vector.normalized()
	elif distance < aifirerange:
		velocity = -distance_vector
	
	
	if randangle != 0:
		Player2.velocity = velocity.rotated(randangle)
	else:
		Player2.velocity = velocity
	
	#fire
	if distance <= aifirerange:
		Player2.fire()


"""----- MEDIUM ai code and functions -----"""

func find_aim_vect(a,b,c,aiming_at_distance,aiming_at_speed):
	var D = b*b + a*c
	var impact_time = 0
	if D >= 0:
		impact_time = (b +sqrt(D))/a
		if impact_time < 0:
			impact_time = 0
	var aim_vect = aiming_at_distance + aiming_at_speed*impact_time
	return aim_vect

func medium_ai_behaviour():
	#variables
	targetPos = Player1.position
	aiPos = Player2.position
	distance_vector = targetPos - aiPos
	distance = distance_vector.length()
	distance_norm = distance_vector.normalized()
	velocity = Vector2(0,0)
	
	
	targetdir = Player1.movement
	targetspeed_vect = targetdir*targetmaxspeed
	var a = aibulletspeed*aibulletspeed - targetspeed_vect.dot(targetspeed_vect)
	var b = distance_vector.dot(targetspeed_vect)
	var c = distance_vector.dot(distance_vector)
	var aim_vect = find_aim_vect(a,b,c,distance_vector,targetspeed_vect)
	#angle
	Player2.rotation = aim_vect.angle()
	
	#velocity
	if distance > aifirerange:
		velocity = distance_norm
	elif distance < aifirerange:
		velocity = -distance_norm
	
	if distance <= targetfirerange and !move_like_crazy:
		_on_Random_timeout()
		move_like_crazy = true
	if distance > targetfirerange and move_like_crazy:
		$Random.stop()
		move_like_crazy = false
	
	if randangle != 0:
		Player2.velocity = velocity.rotated(randangle)
	else:
		Player2.velocity = velocity
	#fire
	if aim_vect.length() <= aifirerange:
		Player2.fire()

"""----- HARD ai code and functions -----"""

func hard_ai_behaviour():
	#variables
	targetPos = Player1.position
	aiPos = Player2.position
	distance_vector = targetPos - aiPos
	distance = distance_vector.length()
	distance_norm = distance_vector.normalized()
	velocity = Vector2(0,0)
	
	
	targetdir = Player1.movement
	targetspeed_vect = targetdir*targetmaxspeed
	
	var aim_vect = find_shortest_aiming_vector(targetPos,aiPos,aibulletspeed,targetspeed_vect)
	
	#angle
	Player2.rotation = aim_vect.angle()
	
	#velocity
	if distance > aifirerange:
		velocity = distance_norm
	elif distance < aifirerange:
		velocity = -distance_norm
	
	if distance <= targetfirerange and !move_like_crazy:
		_on_Random_timeout()
		move_like_crazy = true
	if distance > targetfirerange and move_like_crazy:
		$Random.stop()
		move_like_crazy = false
	
	if randangle != 0:
		Player2.velocity = velocity.rotated(randangle)
	else:
		Player2.velocity = velocity
	
	#fire
	if aim_vect.length() <= aifirerange:
		Player2.fire()

func populate_cords_array(base_cord):
	delta_cords_array.append(Vector2(-base_cord.x,-base_cord.y))
	delta_cords_array.append(Vector2(0,-base_cord.y))
	delta_cords_array.append(Vector2(+base_cord.x,-base_cord.y))
	delta_cords_array.append(Vector2(+base_cord.x,+base_cord.y))
	delta_cords_array.append(Vector2(0,+base_cord.y))
	delta_cords_array.append(Vector2(-base_cord.x,+base_cord.y))
	delta_cords_array.append(Vector2(-base_cord.x,0))
	delta_cords_array.append(Vector2(0,0))

func find_shortest_aiming_vector(base_target_pos,base_ai_pos,base_aibspd,base_tgtspdvect):
	
	var a = base_aibspd*base_aibspd - base_tgtspdvect.dot(base_tgtspdvect)
	var temp_distance_vect = Vector2()
	var shortest_aiming_vect = null
	var b = null
	var c = null
	var temp_aim_vect = null
	
	for cord_vect in delta_cords_array:
		temp_distance_vect = (targetPos + cord_vect) - base_ai_pos
		b = temp_distance_vect.dot(base_tgtspdvect)
		c = temp_distance_vect.dot(temp_distance_vect)
		temp_aim_vect = find_aim_vect(a,b,c,temp_distance_vect,base_tgtspdvect)
		if shortest_aiming_vect == null or shortest_aiming_vect.length() > temp_aim_vect.length():
			shortest_aiming_vect = temp_aim_vect
	
	return shortest_aiming_vect
