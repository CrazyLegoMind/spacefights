extends TextureRect

const AI_ONLY_MODE = false
const MAX_GAME_POINTS = 5
const gameover_labels_cont_path = "HUD/GameOverContainer/VBoxContainer"
const HARDNESS = ["Easy","Normal","Hard","Hardcore","Ragequit"]
var screen_size
var player1_pos = Vector2()
var player2_pos = Vector2()
var player1_vel
var player1_score = 0
var player2_score = 0
var player1_connected = false
var player2_connected = false
var ship_1:ShipClass
var ship_2:ShipClass
var current_ai_level = 0
var ai_ship = 0

signal fire1
signal countdown_end

func _ready():
	texture = Global.background_texture
	Global.set_pausable(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	current_ai_level = Global.ai_level
	screen_size = get_viewport_rect().size
	ai_ship = Global.player2_ship
	player1_pos.x = screen_size.x/8
	player1_pos.y = screen_size.y/8
	player2_pos.x = (screen_size.x/8)*7
	player2_pos.y = (screen_size.y/8)*7
	spawn_players()
	stop_ships(true)
	$HUD/ScoreContainer.hide()
	startround()
	yield($".","countdown_end")
	stop_ships(false)


func spawn_players():
	ship_1 = load(Global.ship_number_to_path(Global.player1_ship)).instance()
	ship_1.name = "Player1"
	ship_2 = load(Global.ship_number_to_path(Global.player2_ship)).instance()
	ship_2.name = "Player2"
	ship_1.set_process_input(false)
	ship_2.set_as_ai()
	if(AI_ONLY_MODE): ship_1.set_as_ai()
	add_child(ship_1)
	add_child(ship_2)
	$".".connect("fire1", $Player1,"fire")
	$Player1.connect("shoot", self,"spawn_bullet")
	$Player1.connect("destroyed", $".","score_update",[],CONNECT_ONESHOT)
	player1_connected = true
	$Player2.connect("shoot", self,"spawn_bullet")
	$Player2.connect("destroyed", $".","score_update",[],CONNECT_ONESHOT)
	player2_connected = true
	
	$AiController.set_players_node($Player1,$Player2)
	$AiController.initialize_ai_data()
	$AiController.set_ai_level(current_ai_level)
	if(AI_ONLY_MODE):
		$AiController2.set_players_node($Player1,$Player2)
		$AiController2.initialize_ai_data()
	ship_1.show_hp_bar(true)
	ship_2.show_hp_bar(true)


func spawn_bullet(bullet):
	add_child(bullet)


func reset_players():
	$Player1.start(player1_pos)
	$Player2.start(player2_pos)
	$Player2.rotation = PI
	$Player1.rotation = 0
	if(!player1_connected):
		$Player1.connect("destroyed", $".","score_update",[],CONNECT_ONESHOT)
		player1_connected = true
	if(!player2_connected):
		$Player2.connect("destroyed", $".","score_update",[],CONNECT_ONESHOT)
		player2_connected = true


func score_update(player):
	stop_ships(true)
	if(player == $Player1):
		player1_connected = false
		$Player2.call_deferred("end",false)
		player2_score += 1
		$HUD/ScoreContainer/VBoxContainer/HBoxContainer/Score2.text = str(player2_score)
	else:
		player2_connected = false
		$Player1.call_deferred("end",false)
		player1_score += 1
		$HUD/ScoreContainer/VBoxContainer/HBoxContainer/Score1.text = str(player1_score)
	
	if player1_score >= MAX_GAME_POINTS or player2_score >= MAX_GAME_POINTS:
		gameover()
		return
	else:
		startround()
		yield($".","countdown_end")
		stop_ships(false)


func startround():
	$HUD/ScoreContainer.show()
	var Counter = $HUD/ScoreContainer/VBoxContainer/Counter
	$Timer.start()
	yield($Timer, "timeout")
	Counter.text = "2"
	yield($Timer, "timeout")
	Counter.text = "1"
	reset_players()
	yield($Timer, "timeout")
	$Timer.stop()
	$HUD/ScoreContainer.hide()
	Counter.text = "3"
	emit_signal("countdown_end")


func gameover():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var winnermsg_node = get_node(gameover_labels_cont_path+"/WinnerMessage")
	if player1_score == player2_score:
		winnermsg_node.text = "DRAW GAME"
	elif player1_score > player2_score:
		if AI_ONLY_MODE:
			winnermsg_node.text = "AI WINS VS AI... WHAT??? " +str(player1_score)+ "-"+str(player2_score)
		else:
			winnermsg_node.text = "YOU DEFEATED THE AI " +str(player1_score)+ "-"+str(player2_score)
			update_local_progress()
		
	else:
		if AI_ONLY_MODE:
			winnermsg_node.text = "AI WINS VS AI... WHAT??? " +str(player1_score)+ "-"+str(player2_score)
		else:
			winnermsg_node.text = HARDNESS[current_ai_level] + " AI WINS " +str(player1_score)+ "-"+str(player2_score)
	
	$HUD/GameOverContainer.show()


func update_local_progress():
	var localmode_dict = Global.get_client_stat("LocalMode")
	var localmode_defeatedlist = localmode_dict["Defeated"]
	var unlock_node = get_node(gameover_labels_cont_path+"/UnlockMessage")
	if current_ai_level != localmode_dict["Unlocked"]:
		unlock_node.text = "Do you feel powerful now? poor AI..."
		return
	
	
	localmode_defeatedlist[ai_ship] = 1
	Global.set_client_stat("LocalMode","Defeated", localmode_defeatedlist)
	var sum = 0
	var ship_to_defeat = " - "
	for ship in range(Global.SHIP_AMOUNT):
		sum += localmode_defeatedlist[ship]
		if localmode_defeatedlist[ship] == 0:
			ship_to_defeat += Global.ship_names[ship] + " - "
	
	if current_ai_level+1 < HARDNESS.size():
		if sum == Global.SHIP_AMOUNT:
			unlock_node.text = "Congratulations, you unlocked " + HARDNESS[current_ai_level+1] + " AI difficulty"
			Global.set_client_stat("LocalMode","Unlocked",current_ai_level+1)
			for i in range(localmode_defeatedlist.size()):
				localmode_defeatedlist[i] = 0
		else:
			unlock_node.text = "there are ships to defeat on "+ HARDNESS[current_ai_level]+" difficulty to unlock "+HARDNESS[current_ai_level+1]+" difficulty \n"+ship_to_defeat
	else:
		unlock_node.text = "All AI difficulties already unlocked"
	
	Global.set_client_stat("LocalMode","Defeated", localmode_defeatedlist)


func stop_ships(value:bool):
	$Player1.set_process_input(!value)
	$AiController.set_active(!value)
	if(AI_ONLY_MODE):
		$AiController2.set_active(!value)


func _on_Restart_pressed():
	Global.reload_scene()


func _on_ToMenu_pressed():
	Global.goto_scene("res://scenes/mainmenu/MainMenu.tscn")
