extends Node

var Player1 = null
var Player2 = null
var aifirerange
var targetfirerange
var targetmaxspeed 
var aibulletspeed
var randangle = 0
var move_like_crazy = false

#modes
enum HARDNESS {
	EASY = 0,
	NORMAL = 1,
	HARD = 2,
	HARDCORE = 3,
	RAGEQUIT = 4
}
var mode = 0
var active = false

#easy difficulty variables
var targetPos = Vector2(0,0)
var aiPos  = Vector2(0,0)
var distance_vector = Vector2(0,0)
var distance = 0
var distance_norm = Vector2(0,0)
var velocity = Vector2(0,0)

#medium difficulty variables
var targetdir = Vector2(0,0)
var targetspeed_vect = Vector2(0,0)

func _ready():
	pass

func initialize_ai_data():
	aifirerange = Player2.firerange
	targetfirerange = Player1.firerange
	aibulletspeed = Player2.bulletspeedmodifier*600
	targetmaxspeed = Player1.speed

func set_active(value:bool):
	active = value

func set_ai_level(value):
	mode = value

func set_players_node(player1_node,player2_node):
	Player1 = player2_node
	Player2 = player1_node

func _on_Random_timeout():
	var delay_time = (randi()%100+1)/100.0
	$Random.wait_time = delay_time
	$Random.start()
	var mov_type = randf()*100
	if mov_type < 10 : #charge at the opponent 10% chance
		randangle = 0
	elif mov_type < 55  : #move right 45% chance
		randangle = rand_range(PI/4,3*(PI/4))
	else: #move left 45% chance 
		randangle = rand_range(-PI/4,-3*(PI/4))
	
	$RandomEnd.start()

func _on_RandomEnd_timeout():
	randangle = 0

#warning-ignore:unused_argument
func _process(delta):
	if(!active):
		return
	if mode == HARDNESS.EASY:
		easy_ai_behaviour()
	elif mode == HARDNESS.NORMAL:
		medium_ai_behaviour()

func easy_ai_behaviour():
	#variables
	targetPos = Player1.position
	aiPos = Player2.position
	distance_vector = targetPos - aiPos
	distance = distance_vector.length()
	velocity = Vector2(0,0)
	
	#angle
	Player2.rotation = distance_vector.angle()
	
	#velocity
	if distance > aifirerange:
		velocity = distance_vector.normalized()
	elif distance < aifirerange:
		velocity = -distance_vector
	
	if distance <= targetfirerange*1.1 and !move_like_crazy:
		_on_Random_timeout()
		move_like_crazy = true
	if distance > targetfirerange*1.1 and move_like_crazy:
		$Random.stop()
		move_like_crazy = false
	
	if randangle != 0:
		Player2.velocity = velocity.rotated(randangle)
	else:
		Player2.velocity = velocity
	
	#fire
	if distance <= aifirerange:
		Player2.fire()

func medium_ai_behaviour():
	#variables
	targetPos = Player1.position
	aiPos = Player2.position
	distance_vector = targetPos - aiPos
	distance = distance_vector.length()
	distance_norm = distance_vector.normalized()
	velocity = Vector2(0,0)
	
	
	targetdir = Player1.movement
	targetspeed_vect = targetdir*targetmaxspeed
	var a = aibulletspeed*aibulletspeed - targetspeed_vect.dot(targetspeed_vect)
	var b = distance_vector.dot(targetspeed_vect)
	var c = distance_vector.dot(distance_vector)
	
	var D = b*b + a*c
	var impact_time = 0
	if D >= 0:
		impact_time = (b +sqrt(D))/a
		if impact_time < 0:
			impact_time = 0
	var aim_vect = distance_vector + targetspeed_vect*impact_time
	
	#angle
	Player2.rotation = aim_vect.angle()
	
	#velocity
	if distance > aifirerange:
		velocity = distance_norm
	elif distance < aifirerange:
		velocity = -distance_norm
	
	if distance <= targetfirerange and !move_like_crazy:
		_on_Random_timeout()
		move_like_crazy = true
	if distance > targetfirerange and move_like_crazy:
		$Random.stop()
		move_like_crazy = false
	
	if randangle != 0:
		Player2.velocity = velocity.rotated(randangle)
	else:
		Player2.velocity = velocity
	
	#fire
	if aim_vect.length() <= aifirerange:
		Player2.fire()
